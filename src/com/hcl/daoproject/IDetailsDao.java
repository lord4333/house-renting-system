package com.hcl.daoproject;
import java.util.List;

import com.hcl.modelproject.Details;
import com.hcl.modelproject.HouseInfo;

public interface IDetailsDao {
	public int detailsAuthentication(Details details);
	public List<HouseInfo> viewAllHouse();
    public int addHouse(HouseInfo info);
    public int editAddr(HouseInfo info);
    public int editNameMob(HouseInfo info);
    public int editNameMobAddr(HouseInfo info); 
    public int removeHouse(HouseInfo info);
}
