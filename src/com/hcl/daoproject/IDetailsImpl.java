package com.hcl.daoproject;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.hcl.modelproject.Details;
import com.hcl.modelproject.HouseInfo;
import com.hcl.util.Db;
import com.hcl.util.Query;


public class IDetailsImpl implements IDetailsDao{
 PreparedStatement pst;
 ResultSet rs;
 int result;
 
	@Override
	public int detailsAuthentication(Details details) {
		result=0;
		
		try {
			pst=Db.getConnection().prepareStatement(Query.detailsAuth);
			pst.setString(1,details.getUsername());
			pst.setString(2,details.getPassword());
			rs=pst.executeQuery();
			while(rs.next()) 
			{
			
			result++;
			
			}
		}catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occurs in  detailsAuthentication ");
		}
		return result;
	}

	@Override
	public List<HouseInfo> viewAllHouse() {
		List<HouseInfo> list=new ArrayList<HouseInfo>();
	try {
		pst=Db.getConnection().prepareStatement(Query.viewAll);
		rs=pst.executeQuery();
		while(rs.next()) {
		HouseInfo info=new HouseInfo(rs.getString(1), rs.getLong("mobilenumber"), rs.getString(3), rs.getString(4),
				rs.getString(5),rs.getString(6), rs.getString(7),rs.getInt(8));
		
		
		list.add(info);
		
		
		}
		
	} catch (ClassNotFoundException | SQLException e) {

		System.out.println("Exception Occurs in View All Houses");
		
	} finally {
		
		try {
			Db.getConnection().close();
		
			pst.close();
		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		}
	}
	
		return list;
	}

	
	@Override
	public int addHouse(HouseInfo info) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.addHouse);
			pst.setString(1,info.getName());
			pst.setLong(2,info.getMobilenumber());
			pst.setString(3,info.getAddress());
			pst.setString(4,info.getLocation());
			pst.setString(5,info.getHousesize());
			pst.setString(6, info.getBhk());
			pst.setString(7,info.getFacilities());
			pst.setInt(8,info.getId());
			result=pst.executeUpdate();
			
				}
		catch (ClassNotFoundException | SQLException e) {
			
			System.out.println("Exception occurs in the add House");
			e.printStackTrace();
		}
		finally {
			try {
				Db.getConnection().close();
				pst.close();
			}catch (ClassNotFoundException | SQLException e) {
				
			}
			
		}
		
	
		return result;
	}

	@Override
	public int editAddr(HouseInfo info) {
		result=0;
			try {
				pst=Db.getConnection().prepareStatement(Query.editAddr);
				pst.setString(1, info.getAddress());
				pst.setInt(2, info.getId());
				result=pst.executeUpdate();
			} catch (ClassNotFoundException | SQLException e) {
				System.out.println("Exception Occours in Edit Address");
			} finally {
				try {
					Db.getConnection().close();	
					pst.close();
				} catch (ClassNotFoundException | SQLException e) {
					
				}
			}
			
		
		return result;
	}

	@Override
	public int editNameMob(HouseInfo info) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.editNameMob);
			pst.setString(1, info.getName());
			pst.setLong(2, info.getMobilenumber());
			pst.setInt(3, info.getId());
			result=pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occours in Edit Name and Mobile Number");
		} finally {
			try {
				Db.getConnection().close();	
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
				
			}
		}
		
		return result;
	}

	@Override
	public int editNameMobAddr(HouseInfo info) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.editNameMobAddr);
			pst.setString(1, info.getName());
			pst.setLong(2, info.getMobilenumber());
			pst.setString(3, info.getAddress());
			pst.setInt(4, info.getId());
			result=pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occours in Edit Name Mobile Number Address");
		} finally {
			try {
				Db.getConnection().close();	
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
				
			}
		}
		
		return result;
	}

	@Override
	public int removeHouse(HouseInfo info) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.removeHouse);
			pst.setInt(1, info.getId());
			result=pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occours in Remove House Details");
		} finally {
			try {
				Db.getConnection().close();	
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
				
			}
		}
		
		return result;
	}
}

	
