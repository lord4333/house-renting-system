package com.hcl.util;

public class Query {
public static String detailsAuth="Select * from details Where username=? and password=?";
public static String viewAll="Select * from houseinfo";  
public static String addHouse="insert into HouseInfo values(?,?,?,?,?,?,?,?)"; 
public static String editAddr="update HouseInfo set address=? where id=?";
public static String editNameMob="update HouseInfo set name=?,mobilenumber=? where id=?";
public static String editNameMobAddr="update HouseInfo set name=?,mobilenumber=?,address=? where id=?";
public static String removeHouse="delete from HouseInfo where id=?";
}
