package com.hcl.view;

import java.util.Scanner;
import java.util.List;

import com.hcl.controller.DetailsController;
import com.hcl.modelproject.HouseInfo;


public class Main {
	
	public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Username");
		String username=sc.nextLine();
		System.out.println("Enter Password");
		String password=sc.next();
		DetailsController controller=new DetailsController();
		int result=0;
		result=controller.detailsAuthentication(username,password);
		if(result>0) {
		System.out.println("Hi "+username+" Welcome to Admin Page");
		int cont=0;
		do {
			
		
		System.out.println("1.Add House Details 2.Remove House Details 3.Edit House Details 4.View All the House Details");
		int option=sc.nextInt();
		sc.nextLine();
		switch(option) {
		case 1:
			System.out.println("Enter name");
			String name=sc.nextLine();
			
			System.out.println("Enter Mobile Number");
			long mobilenumber=sc.nextLong();
			sc.nextLine();
			System.out.println("Enter Address");
			String address=sc.nextLine();
			
			System.out.println("Enter Location");
			String location=sc.nextLine();
			System.out.println("Enter House size in SquareFeet");
			String housesize=sc.nextLine();
			System.out.println("Enter No of BHK");
			String bhk=sc.nextLine();
			System.out.println("Enter Facilities");
			String facilities=sc.nextLine();
			System.out.println("Enter House ID");
		    Integer id=sc.nextInt();
			sc.nextLine();
		    result=controller.addHouse(name, mobilenumber, address, location, housesize, bhk, facilities,id);
			if(result>0) {
				System.out.println(name+" Added Successfilly");
			}
				
			break;
		case 2:
			System.out.println("Enter House ID to Remove Details");
			id=sc.nextInt();
			result=controller.removeHouse(id);
			System.out.println((result>0)? id + " Removed Successfully" : id + " Not Removed Successfully");
			break;
		case 3:
			System.out.println("1.Edit Address 2.Edit Name,Mobile nuber 3.Edit Name,Mobile number,Address");
			int options=sc.nextInt();
			sc.nextLine();
			switch(options) {
			case 1:
				System.out.println("Enter Address");
				address=sc.nextLine();
				System.out.println("Enter House ID");
				id=sc.nextInt();
				sc.nextLine();
				result=controller.editAddr(address,id);
				System.out.println((result>0)? id + " Updated Successfully" : id + " Not Updated Successfully");
				break;
			case 2:
				System.out.println("Enter Name");
				name=sc.nextLine();
				System.out.println("Enter Mobile Number");
				mobilenumber=sc.nextLong();
				sc.nextLine();
				System.out.println("Enter House ID");
				id=sc.nextInt();
				sc.nextLine();
				result=controller.editNameMob(name,mobilenumber,id);
				System.out.println((result>0)? id + " Updated Successfully" : id + " Not Updated Successfully");
				break;
			case 3:
				System.out.println("Enter Name");
				name=sc.nextLine();
				System.out.println("Enter Mobile Number");
				mobilenumber=sc.nextLong();
				sc.nextLine();
				System.out.println("Enter Address");
				address=sc.nextLine();
				System.out.println("Enter House ID");
				id=sc.nextInt();
				
				result=controller.editNameMobAddr(name,mobilenumber,address,id);
				System.out.println((result>0)? id + " Updated Successfully" : id + " Not Updated Successfully");
				break;
				default:
					System.out.println("Invalid option");
			}
			break;
		case 4:
			List<HouseInfo> list=controller.viewAllHouse();		
			if(list.size()>0) {
				System.out.println("name,mobilenumber,address,location,housesize,bhk,facilities,id");
			for (HouseInfo houseInfo: list) {
				System.out.println(houseInfo.getName()+","+houseInfo.getMobilenumber()+","+houseInfo.getAddress()+","+
						houseInfo.getLocation()+","+houseInfo.getHousesize()+","+houseInfo.getBhk()+","+houseInfo.getFacilities()+","+houseInfo.getId());
				
				
				
			}
			}
			else {
				System.out.println("No Records Found");
			}
		    break;
		default:
			System.out.println("Invalid Selection");
			
		}
		System.out.println("Would You Like to continue press 1");
        cont=sc.nextInt();
		}while(cont == 1);
		
		}
		else
		{
			System.out.println("Username or Password is Incorrect");
		}
        System.out.println("Done");
		sc.close();
	}

}
