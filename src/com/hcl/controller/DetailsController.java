package com.hcl.controller;

import java.util.List;

import com.hcl.daoproject.IDetailsDao;
import com.hcl.daoproject.IDetailsImpl;
import com.hcl.modelproject.Details;
import com.hcl.modelproject.HouseInfo;

public class DetailsController {
	int result;
	IDetailsDao dao=new IDetailsImpl();
	
	
	public int detailsAuthentication(String username,String password) {
	
	Details details=new Details(username,password);
	return dao.detailsAuthentication(details);
	
	}
	public List<HouseInfo> viewAllHouse(){
		return dao.viewAllHouse();
		
	}
	public int addHouse(String name,Long mobilenumber,String address,String location,
			String housesize,String bhk,String facilities,Integer id)
	{
		HouseInfo info=new HouseInfo(name,mobilenumber,address,location,housesize,bhk,facilities,id);
		return dao.addHouse(info);
	}
	public int editAddr(String address,Integer id) {
		
		HouseInfo info=new HouseInfo();
		info.setAddress(address);
		info.setId(id);
		return dao.editAddr(info);
	}
	public int editNameMob(String name,Long mobilenumber,Integer id) {
		
		HouseInfo info=new HouseInfo();
		info.setName(name);
		info.setMobilenumber(mobilenumber);
		info.setId(id);
		return dao.editNameMob(info);	
	}
	public int editNameMobAddr(String name,Long mobilenumber,String address,Integer id) {
		HouseInfo info=new HouseInfo();
		info.setName(name);
		info.setMobilenumber(mobilenumber);
		info.setAddress(address);
		info.setId(id);
		return dao.editNameMobAddr(info);
	}
	public int removeHouse(int id) {
		HouseInfo info=new HouseInfo();
		info.setId(id);
		return dao.removeHouse(info);
	}
	}

